FROM roundcube/roundcubemail:1.6.x-apache
MAINTAINER Andrej Ramašeuski <andrej.ramaseuski@pirati.cz>

RUN set -ex; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        git \
    ; \
    \
    composer \
        --working-dir=/usr/src/roundcubemail/ \
        --prefer-dist \
        --prefer-stable \
        --update-no-dev \
        --no-interaction \
        --optimize-autoloader \
        require \
            roundcube/classic \
            johndoh/contextmenu \
            prodrigestivill/gravatar \
    ;

COPY roundcube_logo.png /var/www/html/skins/classic/images
COPY login.html /var/www/html/skins/classic/templates
